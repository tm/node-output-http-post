var fs = require("fs");
var host = "127.0.0.1";
var port = 1337;
var express = require("express");
var http = require('http')

var app = express();




app.configure(function(){
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router); //use both root and other routes below
  app.use(express.static(__dirname + "/public")); //use static files in ROOT/public folder
});


app.get("/", function(request, response){ //root dir
    response.send("Hello!!");
});

app.post("/output", function(req, res){ //root dir
    console.log(req.body);
    res.send(JSON.stringify(req.body));
});

app.listen(port, host);
console.log('Server running on ' + host + ':' + port);
