# Basic Node.js HTTP Server to Output HTTP POSTs

This node.js application runs at the console and will output to the console any HTTP POST requests sent to it.

## Requirements

* Node.js

## How to use

1. Clone repository
2. Change to directory where repository is located
3. Install packages by typing `npm install`
4. Run server by typing `node app.js`
5. Perform HTTP POST methods to `http://localhost:1337/output`. I suggest using [Postman](http://www.getpostman.com) for this